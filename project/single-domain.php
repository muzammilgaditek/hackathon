<?php
/**
 * Hackathon
 */

require_once('header.php');
require_once('inc/single_domain.php');
?>

<?php
    $get_one_domain = get_single_domain();
?>
    <div class="container">
        <h1>Add Domain Records</h1>
        <h6 class="hck_dmn_id"><?= $get_one_domain->id ?></h6>
        <h3 class="hck_dmn_name"><?= $get_one_domain->name ?></h3>

        <div class="hck_upd_rcrd_wrap">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Type</th>
                        <th scope="col">Value</th>
                        <th scope="col">TTL</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" class="form-control" id="hck_rcrd_name" placeholder="www"></td>
                        <td>
                            <div class="form-group">
                                <select class="form-control"  id="hck_rcrd_option">
                                    <option value="A">A Record</option>
                                    <option value="CNAME">CNAME</option>
                                    <option value="TXT">TXT</option>
                                    <option value="MX">MX</option>
                                    <option value="AAAA">AAAA</option>
                                    <option value="ANAME">ANAME</option>
                                </select>
                            </div>
                        </td>
                        <td><input type="text" class="form-control" id="hck_rcrd_value" placeholder="8.8.8.8"></td>
                        <td><input type="text" class="form-control" id="hck_rcrd_ttl" placeholder="3600"></td>
                        <td><a href="javascript:void(0);" class="btn btn-info" id="hck_üpd_rcrd_btn">Update</a></td>
                    </tr>
                </tbody>
            </table>

            <div class="success_msg_wrap alert alert-success"></div>
        </div>
    </div>
<?php require_once('footer.php');?>
