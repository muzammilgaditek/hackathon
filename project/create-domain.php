<?php
    require_once('header.php');
?>

<div class="container">

    <div class="hck_crt_wrap">
        <h1>Create Domain</h1>
        <form action="">
            <div class="form-group">
                <input type="text" class="form-control" id="get_user_domain">
                <div class="error_box"></div>
            </div>
            <button type="submit" class="btn btn-default" id="create_domain_btn">Submit</button>
        </form>

        <div class="success_msg_wrap alert alert-success"></div>
    </div>
</div>

<?php require_once('footer.php'); ?>