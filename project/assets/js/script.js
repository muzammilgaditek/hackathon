$('document').ready(function () {
    //"use strict";

    $('#create_domain_btn').on('click', function (e) {
       e.preventDefault();

        var get_user_domain = $('#get_user_domain').val();
        var get_error_box = $('.error_box');
        var succ_msg_box = $('.success_msg_wrap');

       get_error_box.show();

       if(get_user_domain === '')
       {
            $('.error_box').text('Please enter your domain name.');

            setTimeout(function () {
                get_error_box.hide();
            }, 1000);

            return false;
       }

        get_error_box.hide();

        console.log('Its Pass1!');

       $.ajax({
           type: "POST",
           url: 'inc/create_domain.php',
           data: {
               get_user_domain: get_user_domain,
           },
           success: function(response)
           {
               succ_msg_box.show();
               succ_msg_box.text('Domain has been created.');
               setTimeout(function () {
                   succ_msg_box.hide();
                   location.replace("index.php");
               }, 2000);

               $('#get_user_domain').val('');
               console.log('Its Pass2!');
           }
       })
    });


    $('.hck_upd_rcrd_btn2').on('click', function () {
       let get_current_id = $(this).data('id');
       location.replace("single-domain.php?get_current_id=" + get_current_id);
    });

    $('.hck_dlt_rcrd_btn').on('click', function () {
        let get_current_id = $(this).data('id');
        let get_this_node = $(this).parent().parent();

        var succ_msg_box = $('.success_msg_wrap');

        if (confirm("Are you sure you want to Delete this domain?")) {
            get_this_node.remove();
        } else {
            return false;
        }

        $.ajax({
            type: "POST",
            url: 'inc/delete_domain.php',
            data: {
                get_domain_id: get_current_id,
            },
            success: function(response)
            {
                succ_msg_box.show();
                succ_msg_box.text('Domain has been deleted successfully');
                setTimeout(function () {
                    succ_msg_box.hide();
                }, 2000);

                console.log(response);
            }
        })
    });

    $('#hck_üpd_rcrd_btn').on('click', function () {
        let get_domain_id = $('.hck_dmn_id').text();
        let get_record_name = $('#hck_rcrd_name').val();
        let get_record_option = $('#hck_rcrd_option option:selected').val();
        let get_record_value = $('#hck_rcrd_value').val();
        let get_record_ttl = $('#hck_rcrd_ttl').val();

        var succ_msg_box = $('.success_msg_wrap');

        $.ajax({
            type: "POST",
            url: 'inc/update_domain.php',
            data: {
                get_domain_id: get_domain_id,
                get_record_name: get_record_name,
                get_record_option: get_record_option,
                get_record_value: get_record_value,
                get_record_ttl: get_record_ttl,
            },
            success: function(response)
            {
                succ_msg_box.show();
                succ_msg_box.text('DNS has been updated successfully');

                setTimeout(function () {
                    succ_msg_box.hide();
                    location.replace("index.php");
                }, 2000);

                $('#hck_rcrd_name, #hck_rcrd_value, #hck_rcrd_ttl').val('');

                console.log(response);
            }
        })
    });
});