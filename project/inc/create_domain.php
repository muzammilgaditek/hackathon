<?php
    require_once('DnsMadeEasyHelper.php');

    function create_domain()
    {
        $crt_domain = new DnsMadeEasy();
        $get_user_domain = '';

        if(isset($_POST['get_user_domain']))
        {
            $get_user_domain = $_POST['get_user_domain'];
        }

        $params = array(
            'name' => $get_user_domain,
        );

        $crt_domain->callDME('', 'POST', $params);
    }

    create_domain();
?>