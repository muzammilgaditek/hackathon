<?php
/**
 * Hackathon
 */


/**
 * Class DnsMadeEasy
 */
class DnsMadeEasy
{
    private $DNS_MADE_EASY_URL = 'https://api.sandbox.dnsmadeeasy.com/V2.0/dns/managed/';
    private $DNS_MADE_EASY_API_KEY = '8867adb6-65d3-4375-8c76-788ff9778919';
    private $DNS_MADE_EASY_SECRET_KEY = 'b2d02765-7669-4bcb-9814-a44ee2f255fd';

    public function get_date()
    {
        return gmdate('D, d M Y H:i:s T');
    }

    public function callDME($url, $method, $params = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->DNS_MADE_EASY_URL . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST , $method);

        if($params){
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        }

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            "Content-Type: application/json",
            'x-dnsme-hmac:' . $this->get_hmac_date(),
            'x-dnsme-apiKey:' . $this->DNS_MADE_EASY_API_KEY,
            'x-dnsme-requestDate:' . $this->get_date()
        ]);

        $output = curl_exec($ch);

        //print_r($output);

        //Check Curl request
        if (curl_errno($ch)) {
            die('Couldn\'t send request: ' . $output);
        } else {
            $resultStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if ($resultStatus == 200) {
                //echo('Succesful Curl');
            } else {
                die('Request failed: HTTP status code: ' . $resultStatus);
            }
        }

        curl_close($ch);

        //echo '<pre>'. print_r(json_decode($output), true) . '</pre>';
        return json_decode($output);
    }

    public function get_hmac_date()
    {
        //print_r($this->get_date());

        $date= $this->get_date();
        $algo='SHA1';
        $key='b2d02765-7669-4bcb-9814-a44ee2f255fd';
        $hash=hash_hmac($algo, $date, $key);
        return $hash;
    }
}