<?php
/**
 * Hackathon
 */

require_once('header.php');
?>

<?php
    $crt_domain = new DnsMadeEasy();
    $get_all_data = $crt_domain->callDME('', 'GET');
?>
    <div class="container">
        <div class="hck_domain_wrap">
            <h1>Manage All Domains</h1>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Domain Name</th>
                        <th scope="col">Edit Domain</th>
                        <th scope="col">Delete Domain</th>
                    </tr>
                    </thead>
                    <tbody>

<?php
    foreach($get_all_data->data as $v)
    {
?>
                        <tr>
                            <th scope="row"><?= $v->id; ?></th>
                            <td><?= $v->name; ?></td>
                            <td><a href="javascript:void(0);" class="hck_upd_rcrd_btn2" data-id="<?= $v->id; ?>"><i class="fa fa-edit"></i></a></td>
                            <td><a href="javascript:void(0);" class="hck_dlt_rcrd_btn" data-id="<?= $v->id; ?>"><i class="fa fa-trash"></i></a></td>
                        </tr>
<?php
    }
?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="hck_domain_topWrap">
            <a href="create-domain.php" class="btn btn-primary">Create Domain</a>
        </div>
    </div>

<?php require_once('footer.php');?>
